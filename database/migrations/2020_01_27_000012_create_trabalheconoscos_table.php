<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrabalheconoscosTable extends Migration
{
    public function up()
    {
        Schema::create('trabalheconoscos', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nome')->nullable();

            $table->string('telefone')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
