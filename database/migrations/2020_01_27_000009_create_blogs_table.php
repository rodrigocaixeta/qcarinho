<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsTable extends Migration
{
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->increments('id');

            $table->string('titulo')->nullable();

            $table->string('slug')->nullable();

            $table->string('introducao')->nullable();

            $table->longText('conteudo')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
