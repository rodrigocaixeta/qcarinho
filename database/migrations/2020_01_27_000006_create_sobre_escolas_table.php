<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSobreEscolasTable extends Migration
{
    public function up()
    {
        Schema::create('sobre_escolas', function (Blueprint $table) {
            $table->increments('id');

            $table->string('titulo')->nullable();

            $table->longText('conteudo')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
