<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContatosTable extends Migration
{
    public function up()
    {
        Schema::create('contatos', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nome')->nullable();

            $table->string('email')->nullable();

            $table->string('telefone')->nullable();

            $table->string('como_nos_conheceu')->nullable();

            $table->longText('mensagem')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
