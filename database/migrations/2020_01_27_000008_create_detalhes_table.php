<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalhesTable extends Migration
{
    public function up()
    {
        Schema::create('detalhes', function (Blueprint $table) {
            $table->increments('id');

            $table->longText('conteudo')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
