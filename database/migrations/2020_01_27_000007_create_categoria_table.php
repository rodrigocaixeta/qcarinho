<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriaTable extends Migration
{
    public function up()
    {
        Schema::create('categoria', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nome')->nullable();

            $table->string('slug')->nullable();

            $table->timestamps();

            $table->softDeletes();
        });
    }
}
