<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToDetalhesTable extends Migration
{
    public function up()
    {
        Schema::table('detalhes', function (Blueprint $table) {
            $table->unsignedInteger('categoria_id')->nullable();

            $table->foreign('categoria_id', 'categoria_fk_749647')->references('id')->on('categoria');
        });
    }
}
