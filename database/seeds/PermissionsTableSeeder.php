<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => '1',
                'title' => 'user_management_access',
            ],
            [
                'id'    => '2',
                'title' => 'permission_create',
            ],
            [
                'id'    => '3',
                'title' => 'permission_edit',
            ],
            [
                'id'    => '4',
                'title' => 'permission_show',
            ],
            [
                'id'    => '5',
                'title' => 'permission_delete',
            ],
            [
                'id'    => '6',
                'title' => 'permission_access',
            ],
            [
                'id'    => '7',
                'title' => 'role_create',
            ],
            [
                'id'    => '8',
                'title' => 'role_edit',
            ],
            [
                'id'    => '9',
                'title' => 'role_show',
            ],
            [
                'id'    => '10',
                'title' => 'role_delete',
            ],
            [
                'id'    => '11',
                'title' => 'role_access',
            ],
            [
                'id'    => '12',
                'title' => 'user_create',
            ],
            [
                'id'    => '13',
                'title' => 'user_edit',
            ],
            [
                'id'    => '14',
                'title' => 'user_show',
            ],
            [
                'id'    => '15',
                'title' => 'user_delete',
            ],
            [
                'id'    => '16',
                'title' => 'user_access',
            ],
            [
                'id'    => '17',
                'title' => 'slideshow_create',
            ],
            [
                'id'    => '18',
                'title' => 'slideshow_edit',
            ],
            [
                'id'    => '19',
                'title' => 'slideshow_show',
            ],
            [
                'id'    => '20',
                'title' => 'slideshow_delete',
            ],
            [
                'id'    => '21',
                'title' => 'slideshow_access',
            ],
            [
                'id'    => '22',
                'title' => 'sobre_escola_create',
            ],
            [
                'id'    => '23',
                'title' => 'sobre_escola_edit',
            ],
            [
                'id'    => '24',
                'title' => 'sobre_escola_show',
            ],
            [
                'id'    => '25',
                'title' => 'sobre_escola_delete',
            ],
            [
                'id'    => '26',
                'title' => 'sobre_escola_access',
            ],
            [
                'id'    => '27',
                'title' => 'escola_access',
            ],
            [
                'id'    => '28',
                'title' => 'categorium_create',
            ],
            [
                'id'    => '29',
                'title' => 'categorium_edit',
            ],
            [
                'id'    => '30',
                'title' => 'categorium_show',
            ],
            [
                'id'    => '31',
                'title' => 'categorium_delete',
            ],
            [
                'id'    => '32',
                'title' => 'categorium_access',
            ],
            [
                'id'    => '33',
                'title' => 'detalhe_create',
            ],
            [
                'id'    => '34',
                'title' => 'detalhe_edit',
            ],
            [
                'id'    => '35',
                'title' => 'detalhe_show',
            ],
            [
                'id'    => '36',
                'title' => 'detalhe_delete',
            ],
            [
                'id'    => '37',
                'title' => 'detalhe_access',
            ],
            [
                'id'    => '38',
                'title' => 'blog_create',
            ],
            [
                'id'    => '39',
                'title' => 'blog_edit',
            ],
            [
                'id'    => '40',
                'title' => 'blog_show',
            ],
            [
                'id'    => '41',
                'title' => 'blog_delete',
            ],
            [
                'id'    => '42',
                'title' => 'blog_access',
            ],
            [
                'id'    => '43',
                'title' => 'contato_create',
            ],
            [
                'id'    => '44',
                'title' => 'contato_edit',
            ],
            [
                'id'    => '45',
                'title' => 'contato_show',
            ],
            [
                'id'    => '46',
                'title' => 'contato_delete',
            ],
            [
                'id'    => '47',
                'title' => 'contato_access',
            ],
            [
                'id'    => '48',
                'title' => 'parceiro_create',
            ],
            [
                'id'    => '49',
                'title' => 'parceiro_edit',
            ],
            [
                'id'    => '50',
                'title' => 'parceiro_show',
            ],
            [
                'id'    => '51',
                'title' => 'parceiro_delete',
            ],
            [
                'id'    => '52',
                'title' => 'parceiro_access',
            ],
            [
                'id'    => '53',
                'title' => 'trabalheconosco_create',
            ],
            [
                'id'    => '54',
                'title' => 'trabalheconosco_edit',
            ],
            [
                'id'    => '55',
                'title' => 'trabalheconosco_show',
            ],
            [
                'id'    => '56',
                'title' => 'trabalheconosco_delete',
            ],
            [
                'id'    => '57',
                'title' => 'trabalheconosco_access',
            ],
        ];

        Permission::insert($permissions);
    }
}
