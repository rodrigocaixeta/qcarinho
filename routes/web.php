<?php

Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes(['register' => false]);
// Admin

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Slideshows
    Route::delete('slideshows/destroy', 'SlideshowController@massDestroy')->name('slideshows.massDestroy');
    Route::post('slideshows/media', 'SlideshowController@storeMedia')->name('slideshows.storeMedia');
    Route::post('slideshows/ckmedia', 'SlideshowController@storeCKEditorImages')->name('slideshows.storeCKEditorImages');
    Route::resource('slideshows', 'SlideshowController');

    // Sobre Escolas
    Route::delete('sobre-escolas/destroy', 'SobreEscolaController@massDestroy')->name('sobre-escolas.massDestroy');
    Route::post('sobre-escolas/media', 'SobreEscolaController@storeMedia')->name('sobre-escolas.storeMedia');
    Route::post('sobre-escolas/ckmedia', 'SobreEscolaController@storeCKEditorImages')->name('sobre-escolas.storeCKEditorImages');
    Route::resource('sobre-escolas', 'SobreEscolaController');

    // Categoria
    Route::delete('categoria/destroy', 'CategoriasController@massDestroy')->name('categoria.massDestroy');
    Route::resource('categoria', 'CategoriasController');

    // Detalhes
    Route::delete('detalhes/destroy', 'DetalhesController@massDestroy')->name('detalhes.massDestroy');
    Route::post('detalhes/media', 'DetalhesController@storeMedia')->name('detalhes.storeMedia');
    Route::post('detalhes/ckmedia', 'DetalhesController@storeCKEditorImages')->name('detalhes.storeCKEditorImages');
    Route::resource('detalhes', 'DetalhesController');

    // Blogs
    Route::delete('blogs/destroy', 'BlogController@massDestroy')->name('blogs.massDestroy');
    Route::post('blogs/media', 'BlogController@storeMedia')->name('blogs.storeMedia');
    Route::post('blogs/ckmedia', 'BlogController@storeCKEditorImages')->name('blogs.storeCKEditorImages');
    Route::resource('blogs', 'BlogController');

    // Contatos
    Route::delete('contatos/destroy', 'ContatoController@massDestroy')->name('contatos.massDestroy');
    Route::resource('contatos', 'ContatoController');

    // Parceiros
    Route::delete('parceiros/destroy', 'ParceirosController@massDestroy')->name('parceiros.massDestroy');
    Route::post('parceiros/media', 'ParceirosController@storeMedia')->name('parceiros.storeMedia');
    Route::post('parceiros/ckmedia', 'ParceirosController@storeCKEditorImages')->name('parceiros.storeCKEditorImages');
    Route::resource('parceiros', 'ParceirosController');

    // Trabalheconoscos
    Route::delete('trabalheconoscos/destroy', 'TrabalheconoscoController@massDestroy')->name('trabalheconoscos.massDestroy');
    Route::post('trabalheconoscos/media', 'TrabalheconoscoController@storeMedia')->name('trabalheconoscos.storeMedia');
    Route::post('trabalheconoscos/ckmedia', 'TrabalheconoscoController@storeCKEditorImages')->name('trabalheconoscos.storeCKEditorImages');
    Route::resource('trabalheconoscos', 'TrabalheconoscoController');
});
