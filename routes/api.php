<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:api']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::apiResource('users', 'UsersApiController');

    // Slideshows
    Route::post('slideshows/media', 'SlideshowApiController@storeMedia')->name('slideshows.storeMedia');
    Route::apiResource('slideshows', 'SlideshowApiController');

    // Sobre Escolas
    Route::post('sobre-escolas/media', 'SobreEscolaApiController@storeMedia')->name('sobre-escolas.storeMedia');
    Route::apiResource('sobre-escolas', 'SobreEscolaApiController');

    // Categoria
    Route::apiResource('categoria', 'CategoriasApiController');

    // Detalhes
    Route::post('detalhes/media', 'DetalhesApiController@storeMedia')->name('detalhes.storeMedia');
    Route::apiResource('detalhes', 'DetalhesApiController');

    // Blogs
    Route::post('blogs/media', 'BlogApiController@storeMedia')->name('blogs.storeMedia');
    Route::apiResource('blogs', 'BlogApiController');

    // Contatos
    Route::apiResource('contatos', 'ContatoApiController');

    // Parceiros
    Route::post('parceiros/media', 'ParceirosApiController@storeMedia')->name('parceiros.storeMedia');
    Route::apiResource('parceiros', 'ParceirosApiController');

    // Trabalheconoscos
    Route::post('trabalheconoscos/media', 'TrabalheconoscoApiController@storeMedia')->name('trabalheconoscos.storeMedia');
    Route::apiResource('trabalheconoscos', 'TrabalheconoscoApiController');
});
