<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/site.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" rel="stylesheet" />
    <title>Q Carinho</title>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-lg-12" style="background: #722e88;background-image: url(images/borda-topo-roxo.png);background-repeat: no-repeat;background-position: left top;min-height: 63px;max-height: 63px;border-radius: 5px;">
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 py-3">
            <div class="logo-menu">
                <img src="{{url('images/logo.png')}}" class="img-fluid pr-5" alt="">
                <div class="menu">
                    <a href="#qcarinho">A Q’Carinho</a>
                    <a href="#servicos">Serviços</a>
                    <a href="#parceiros">Parceiros</a>
                    <a href="#tour">Tour Virtutal</a>
                    <a href="#blog">Blog</a>
                    <a href="#trabalhe">Trabalhe conosco</a>
                    <a href="#contato">Contato</a>
                </div>
            </div>
        </div>
    </div>
</div>

@yield('content')

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
<script src="https://pchen66.github.io/js/three/three.min.js"></script>
<script src="https://rawgit.com/pchen66/panolens.js/dev/build/panolens.min.js"></script>

<script defer>
    Dropzone.options.curriculoDropzone = {
        url: '{{ route('admin.trabalheconoscos.storeMedia') }}',
        maxFilesize: 2, // MB
        maxFiles: 1,
        addRemoveLinks: true,
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        params: {
            size: 2
        },
        success: function (file, response) {
            $('form').find('input[name="curriculo"]').remove()
            $('form').append('<input type="hidden" name="curriculo" value="' + response.name + '">')
        },
        removedfile: function (file) {
            file.previewElement.remove()
            if (file.status !== 'error') {
                $('form').find('input[name="curriculo"]').remove()
                this.options.maxFiles = this.options.maxFiles + 1
            }
        },
        init: function () {
                @if(isset($trabalheconosco) && $trabalheconosco->curriculo)
            var file = {!! json_encode($trabalheconosco->curriculo) !!}
                    this.options.addedfile.call(this, file)
            file.previewElement.classList.add('dz-complete')
            $('form').append('<input type="hidden" name="curriculo" value="' + file.file_name + '">')
            this.options.maxFiles = this.options.maxFiles - 1
            @endif
        },
        error: function (file, response) {
            if ($.type(response) === 'string') {
                var message = response //dropzone sends it's own error messages in string
            } else {
                var message = response.errors.file
            }
            file.previewElement.classList.add('dz-error')
            _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
            _results = []
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                node = _ref[_i]
                _results.push(node.textContent = message)
            }

            return _results
        }
    }
</script>

<script>
    var panorama1, panorama2, panorama3, panorama4, viewer, container, infospot;

    var bar = document.querySelector( '#bar' );

    function onProgressUpdate ( event ) {
        var percentage = event.progress.loaded/ event.progress.total * 100;
        bar.style.width = percentage + "%";
        if (percentage >= 100){
            bar.classList.add( 'hide' );
            setTimeout(function(){
                bar.style.width = 0;
            }, 1000);
        }
    }

    container = document.querySelector( '#container' );

    panorama1 = new PANOLENS.ImagePanorama( '01.jpg' );
    panorama1.addEventListener( 'progress', onProgressUpdate );

    panorama2 = new PANOLENS.ImagePanorama( '02.jpg' );
    panorama2.addEventListener( 'progress', onProgressUpdate );

    panorama3 = new PANOLENS.ImagePanorama( '03.jpg' );
    panorama3.addEventListener( 'progress', onProgressUpdate );

    panorama4 = new PANOLENS.ImagePanorama( '04.jpg' );
    panorama4.addEventListener( 'progress', onProgressUpdate );

    infospot = new PANOLENS.Infospot( 350, PANOLENS.DataImage.Info );

    panorama1.add( infospot );

    viewer = new PANOLENS.Viewer( { container: container } );
    viewer.add( panorama1, panorama2, panorama3, panorama4 );

    // Maunal Set Panorama
    var button1 = document.querySelector( '#btn1' );
    var button2 = document.querySelector( '#btn2' );
    var button3 = document.querySelector( '#btn3' );
    var button4 = document.querySelector( '#btn4' );

    // Enter panorama when load completes
    function onButtonClick( targetPanorama ) {
        bar.classList.remove( 'hide' );
        viewer.setPanorama( targetPanorama );
    }

    button1.addEventListener( 'click', onButtonClick.bind( this, panorama1 ) );

    button2.addEventListener( 'click', onButtonClick.bind( this, panorama2 ) );

    button3.addEventListener( 'click', onButtonClick.bind( this, panorama3 ) );

    button4.addEventListener( 'click', onButtonClick.bind( this, panorama4 ) );
</script>
</body>
</html>
