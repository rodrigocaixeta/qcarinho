@extends('site.template')


@section('content')
    <section>
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                @foreach($slide as $i => $s)
                    <div class="carousel-item @if ($i == 0) active @endif">
                        <img src="{{url($s->imagem->getUrl())}}" class="d-block w-100" alt="...">
                    </div>
                    @endforeach
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>
    <div class="bar">

    </div>

    <section id="qcarinho" style="
    background: #be3e62;
    background-image: url({{url('images/bg-coracao.png')}});
        background-blend-mode: multiply;
" class="py-5">
        <div class="container">
            <div class="row" id="branco">
                <div class="col-lg-12">
                    <h1 class="intro-black-alt">O carinho de casa,</h1>
                    <h2 class="intro-black-alt"> na escola.</h2>
                    <p style="color: #FFF !important;" class="text-white">{!! $qcarinho->conteudo !!}</p>
                </div>
            </div>
        </div>
    </section>

    <section id="servicos">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 p-0 ">
                    <div class="row no-gutters">
                        <div class="col-5 bg-verde py-5" >
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">


                                @foreach ($servicos as $ii => $c)
                                    <a class="nav-link @if ($ii == 0) active @endif" id="v-pills-{{Str::slug($c->categoria->nome, '-')}}-tab" data-toggle="pill" href="#v-pills-{{Str::slug($c->categoria->nome, '-')}}" role="tab" aria-controls="v-pills-{{Str::slug($c->categoria->nome, '-')}}" aria-selected="true">{{$c->categoria->nome}}</a>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="tab-content" id="v-pills-tabContent">
                                @foreach ($servicos as $iii => $s)

                                    <div  class="p-5 tab-pane fade show @if ($iii == 0) active @endif" id="v-pills-{{Str::slug($s->categoria->nome, '-')}}" role="tabpanel" aria-labelledby="v-pills-{{Str::slug($s->categoria->nome, '-')}}-tab">
                                        <h2 class="amarelo">{{$s->categoria->nome}}</h2> <br>
                                        <p><img src="{{url($s->imagem->getUrl())}}" class="img-fluid" alt=""></p>
                                        {!! $s->conteudo !!}
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="tour" style="
        background: #db8f1c;
        background-image: url({{url('images/bg-coracao.png')}});
        background-blend-mode: multiply;
        " class="py-5">
        <div class="text-center text-white">
            <h1>Faça um tour virtual em nossa escola!</h1>
            <p>Conheça um pouco mais da Q’Carinho</p>
        </div>
    </section>

    @include('3d.3d')

    <section id="parceiros">
        <div class="container-fluid">
            <div class="row">
                @foreach ($parceiros as $iiii => $p)
                <div class="col-md-4 py-5 @if ($iiii == 1) cor2 @else cor1 @endif">
                    @if (isset($p->url))
                        <a href="{{$p->url}}">
                            <img src="{{url($p->parceiro->getUrl())}}" class="img-fluid" alt="">
                        </a>
                        @else
                        <img src="{{url($p->parceiro->getUrl())}}" class="img-fluid" alt="">
                    @endif
                </div>
                @endforeach
            </div>
        </div>
    </section>
    
    <section id="blog">
        <div class="container">
            <div class="row py-5">
                @foreach ($blog as $b)

                    <div class="col-md-4">
                        <img src="{{url($b->imagem->getUrl())}}" class="img-fluid" alt="">
                        <a href="#" data-toggle="modal" data-target="#blog{{$b->id}}"><h4>{{$b->titulo}}</h4></a>
                        <p>{{Str::limit($b->introducao, 200, ' (...)')}}</p>
                        <p class="data">{{\Carbon\Carbon::parse($b->created_at)->format('d/m/Y')}}</p>
                        @include('modal.blog')
                    </div>

                @endforeach

            </div>
            <div class="row py-5">
                <div class="col-md-12 text-center">
                    <a href="#" class="btn btn-outline-warning" data-toggle="modal" data-target="#blogs">Visite o nosso
                        blog!</a>
                </div>
            </div>
        </div>

        @include('modal.allblog')
    </section>
    @include('modal.trabalheconosco')
    <section id="trabalhe" style="background: #4cbdb3">
        <div class="container py-5">
            <div class="row">
                <div class="col-md-6 min200 flex-column">
                    <h1>Trabalhe Conosco</h1>
                    <a href="#" class="btn btn-primary btn-block" data-toggle="modal" data-target="#trabalhec">enviar</a>
                </div>
                <div class="col-md-6 min200">
                    <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. </p>
                </div>
            </div>
        </div>
    </section>



    <section id="contato">
        <div class="container">
            <div class="row py-5">
                <div class="col-md-6">
                    <h2 class="amarelo">Onde nos encontrar?</h2>
                    <p>SIG Qd. 06, Lotes 2.230 / 2.240 <br>
                        Setor de Industrias Gráficas</p>
                    <p><i style="color: #dc5d7c;" class="fas fa-phone"></i> <b>(61) 3553-9494</b></p>
                    <p>
                        <img src="{{url('images/mapa.png')}}" class="img-fluid" alt="">
                    </p>
                </div>
                <div class="col-md-6">
                    <h2 class="amarelo">Entre em contato conosco!</h2>
                    <p>Preencha o formulário abaixo para mais informações</p>
                    <form method="POST" action="{{ route("contato") }}" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">

                                    <input placeholder="Nome" class="form-control inputcolor {{ $errors->has('nome') ? 'is-invalid' : '' }}" type="text" name="nome" id="nome" value="{{ old('nome', '') }}">
                                    @if($errors->has('nome'))
                                        <span class="text-danger">{{ $errors->first('nome') }}</span>
                                    @endif
                                    <span class="help-block">{{ trans('cruds.contato.fields.nome_helper') }}</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">

                                    <input placeholder="E-mail" class="form-control inputcolor {{ $errors->has('email') ? 'is-invalid' : '' }}" type="text" name="email" id="email" value="{{ old('email') }}">
                                    @if($errors->has('email'))
                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                    <span class="help-block">{{ trans('cruds.contato.fields.email_helper') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">

                                    <input placeholder="Telefone" class="form-control inputcolor {{ $errors->has('telefone') ? 'is-invalid' : '' }}" type="text" name="telefone" id="telefone" value="{{ old('telefone', '') }}">
                                    @if($errors->has('telefone'))
                                        <span class="text-danger">{{ $errors->first('telefone') }}</span>
                                    @endif
                                    <span class="help-block">{{ trans('cruds.contato.fields.telefone_helper') }}</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">

                                    <select class="form-control inputcolor {{ $errors->has('como_nos_conheceu') ? 'is-invalid' : '' }}" type="text" name="como_nos_conheceu" id="como_nos_conheceu">
                                        <option selected>Como nos Conheceu?</option>
                                        <option value="1">Google</option>
                                    </select>
                                    @if($errors->has('como_nos_conheceu'))
                                        <span class="text-danger">{{ $errors->first('como_nos_conheceu') }}</span>
                                    @endif

                                    <span class="help-block">{{ trans('cruds.contato.fields.como_nos_conheceu_helper') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">

                            <textarea placeholder="Mensagem" rows="5" class="form-control inputcolor {{ $errors->has('mensagem') ? 'is-invalid' : '' }}" name="mensagem" id="mensagem">{{ old('mensagem') }}</textarea>
                            @if($errors->has('mensagem'))
                                <span class="text-danger">{{ $errors->first('mensagem') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.contato.fields.mensagem_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary btn-block" type="submit">
                                Enviar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>



@stop

@section('script')

@stop
