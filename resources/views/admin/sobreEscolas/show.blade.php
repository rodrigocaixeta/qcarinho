@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.sobreEscola.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.sobre-escolas.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.sobreEscola.fields.id') }}
                        </th>
                        <td>
                            {{ $sobreEscola->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.sobreEscola.fields.titulo') }}
                        </th>
                        <td>
                            {{ $sobreEscola->titulo }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.sobreEscola.fields.conteudo') }}
                        </th>
                        <td>
                            {!! $sobreEscola->conteudo !!}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.sobre-escolas.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection