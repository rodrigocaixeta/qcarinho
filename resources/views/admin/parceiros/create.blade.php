@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.parceiro.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.parceiros.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="parceiro">{{ trans('cruds.parceiro.fields.parceiro') }}</label>
                <div class="needsclick dropzone {{ $errors->has('parceiro') ? 'is-invalid' : '' }}" id="parceiro-dropzone">
                </div>
                @if($errors->has('parceiro'))
                    <span class="text-danger">{{ $errors->first('parceiro') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.parceiro.fields.parceiro_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="url">{{ trans('cruds.parceiro.fields.url') }}</label>
                <input class="form-control {{ $errors->has('url') ? 'is-invalid' : '' }}" type="text" name="url" id="url" value="{{ old('url', '') }}">
                @if($errors->has('url'))
                    <span class="text-danger">{{ $errors->first('url') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.parceiro.fields.url_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    Dropzone.options.parceiroDropzone = {
    url: '{{ route('admin.parceiros.storeMedia') }}',
    maxFilesize: 2, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 2,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="parceiro"]').remove()
      $('form').append('<input type="hidden" name="parceiro" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="parceiro"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($parceiro) && $parceiro->parceiro)
      var file = {!! json_encode($parceiro->parceiro) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, '{{ $parceiro->parceiro->getUrl('thumb') }}')
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="parceiro" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}
</script>
@endsection