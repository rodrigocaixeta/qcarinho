@extends('layouts.admin')
@section('content')
@can('parceiro_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.parceiros.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.parceiro.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.parceiro.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Parceiro">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.parceiro.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.parceiro.fields.parceiro') }}
                        </th>
                        <th>
                            {{ trans('cruds.parceiro.fields.url') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($parceiros as $key => $parceiro)
                        <tr data-entry-id="{{ $parceiro->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $parceiro->id ?? '' }}
                            </td>
                            <td>
                                @if($parceiro->parceiro)
                                    <a href="{{ $parceiro->parceiro->getUrl() }}" target="_blank">
                                        <img src="{{ $parceiro->parceiro->getUrl('thumb') }}" width="50px" height="50px">
                                    </a>
                                @endif
                            </td>
                            <td>
                                {{ $parceiro->url ?? '' }}
                            </td>
                            <td>
                                @can('parceiro_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.parceiros.show', $parceiro->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('parceiro_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.parceiros.edit', $parceiro->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('parceiro_delete')
                                    <form action="{{ route('admin.parceiros.destroy', $parceiro->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('parceiro_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.parceiros.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-Parceiro:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection