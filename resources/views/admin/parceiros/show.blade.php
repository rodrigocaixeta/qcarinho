@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.parceiro.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.parceiros.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.parceiro.fields.id') }}
                        </th>
                        <td>
                            {{ $parceiro->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.parceiro.fields.parceiro') }}
                        </th>
                        <td>
                            @if($parceiro->parceiro)
                                <a href="{{ $parceiro->parceiro->getUrl() }}" target="_blank">
                                    <img src="{{ $parceiro->parceiro->getUrl('thumb') }}" width="50px" height="50px">
                                </a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.parceiro.fields.url') }}
                        </th>
                        <td>
                            {{ $parceiro->url }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.parceiros.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection