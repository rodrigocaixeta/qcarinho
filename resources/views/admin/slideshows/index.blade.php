@extends('layouts.admin')
@section('content')
@can('slideshow_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.slideshows.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.slideshow.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.slideshow.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Slideshow">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.slideshow.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.slideshow.fields.imagem') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($slideshows as $key => $slideshow)
                        <tr data-entry-id="{{ $slideshow->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $slideshow->id ?? '' }}
                            </td>
                            <td>
                                @if($slideshow->imagem)
                                    <a href="{{ $slideshow->imagem->getUrl() }}" target="_blank">
                                        <img src="{{ $slideshow->imagem->getUrl('thumb') }}" width="50px" height="50px">
                                    </a>
                                @endif
                            </td>
                            <td>
                                @can('slideshow_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.slideshows.show', $slideshow->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('slideshow_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.slideshows.edit', $slideshow->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('slideshow_delete')
                                    <form action="{{ route('admin.slideshows.destroy', $slideshow->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('slideshow_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.slideshows.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-Slideshow:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection