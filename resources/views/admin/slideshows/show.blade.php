@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.slideshow.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.slideshows.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.slideshow.fields.id') }}
                        </th>
                        <td>
                            {{ $slideshow->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.slideshow.fields.imagem') }}
                        </th>
                        <td>
                            @if($slideshow->imagem)
                                <a href="{{ $slideshow->imagem->getUrl() }}" target="_blank">
                                    <img src="{{ $slideshow->imagem->getUrl('thumb') }}" width="50px" height="50px">
                                </a>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.slideshows.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection