@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.slideshow.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.slideshows.update", [$slideshow->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="imagem">{{ trans('cruds.slideshow.fields.imagem') }}</label>
                <div class="needsclick dropzone {{ $errors->has('imagem') ? 'is-invalid' : '' }}" id="imagem-dropzone">
                </div>
                @if($errors->has('imagem'))
                    <span class="text-danger">{{ $errors->first('imagem') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.slideshow.fields.imagem_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    Dropzone.options.imagemDropzone = {
    url: '{{ route('admin.slideshows.storeMedia') }}',
    maxFilesize: 2, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 2,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="imagem"]').remove()
      $('form').append('<input type="hidden" name="imagem" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="imagem"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($slideshow) && $slideshow->imagem)
      var file = {!! json_encode($slideshow->imagem) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, '{{ $slideshow->imagem->getUrl('thumb') }}')
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="imagem" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}
</script>
@endsection