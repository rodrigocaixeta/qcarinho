<div class="m-3">
    @can('detalhe_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.detalhes.create") }}">
                    {{ trans('global.add') }} {{ trans('cruds.detalhe.title_singular') }}
                </a>
            </div>
        </div>
    @endcan
    <div class="card">
        <div class="card-header">
            {{ trans('cruds.detalhe.title_singular') }} {{ trans('global.list') }}
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class=" table table-bordered table-striped table-hover datatable datatable-Detalhe">
                    <thead>
                        <tr>
                            <th width="10">

                            </th>
                            <th>
                                {{ trans('cruds.detalhe.fields.id') }}
                            </th>
                            <th>
                                {{ trans('cruds.detalhe.fields.categoria') }}
                            </th>
                            <th>
                                {{ trans('cruds.detalhe.fields.imagem') }}
                            </th>
                            <th>
                                &nbsp;
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($detalhes as $key => $detalhe)
                            <tr data-entry-id="{{ $detalhe->id }}">
                                <td>

                                </td>
                                <td>
                                    {{ $detalhe->id ?? '' }}
                                </td>
                                <td>
                                    {{ $detalhe->categoria->nome ?? '' }}
                                </td>
                                <td>
                                    @if($detalhe->imagem)
                                        <a href="{{ $detalhe->imagem->getUrl() }}" target="_blank">
                                            <img src="{{ $detalhe->imagem->getUrl('thumb') }}" width="50px" height="50px">
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    @can('detalhe_show')
                                        <a class="btn btn-xs btn-primary" href="{{ route('admin.detalhes.show', $detalhe->id) }}">
                                            {{ trans('global.view') }}
                                        </a>
                                    @endcan

                                    @can('detalhe_edit')
                                        <a class="btn btn-xs btn-info" href="{{ route('admin.detalhes.edit', $detalhe->id) }}">
                                            {{ trans('global.edit') }}
                                        </a>
                                    @endcan

                                    @can('detalhe_delete')
                                        <form action="{{ route('admin.detalhes.destroy', $detalhe->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                        </form>
                                    @endcan

                                </td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('detalhe_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.detalhes.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-Detalhe:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection