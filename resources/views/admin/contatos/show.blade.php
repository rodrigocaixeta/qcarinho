@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.contato.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.contatos.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.contato.fields.id') }}
                        </th>
                        <td>
                            {{ $contato->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.contato.fields.nome') }}
                        </th>
                        <td>
                            {{ $contato->nome }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.contato.fields.email') }}
                        </th>
                        <td>
                            {{ $contato->email }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.contato.fields.telefone') }}
                        </th>
                        <td>
                            {{ $contato->telefone }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.contato.fields.como_nos_conheceu') }}
                        </th>
                        <td>
                            {{ $contato->como_nos_conheceu }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.contato.fields.mensagem') }}
                        </th>
                        <td>
                            {{ $contato->mensagem }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.contatos.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection