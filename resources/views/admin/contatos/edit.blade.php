@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.contato.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.contatos.update", [$contato->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="nome">{{ trans('cruds.contato.fields.nome') }}</label>
                <input class="form-control {{ $errors->has('nome') ? 'is-invalid' : '' }}" type="text" name="nome" id="nome" value="{{ old('nome', $contato->nome) }}">
                @if($errors->has('nome'))
                    <span class="text-danger">{{ $errors->first('nome') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.contato.fields.nome_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="email">{{ trans('cruds.contato.fields.email') }}</label>
                <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="text" name="email" id="email" value="{{ old('email', $contato->email) }}">
                @if($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.contato.fields.email_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="telefone">{{ trans('cruds.contato.fields.telefone') }}</label>
                <input class="form-control {{ $errors->has('telefone') ? 'is-invalid' : '' }}" type="text" name="telefone" id="telefone" value="{{ old('telefone', $contato->telefone) }}">
                @if($errors->has('telefone'))
                    <span class="text-danger">{{ $errors->first('telefone') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.contato.fields.telefone_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="como_nos_conheceu">{{ trans('cruds.contato.fields.como_nos_conheceu') }}</label>
                <input class="form-control {{ $errors->has('como_nos_conheceu') ? 'is-invalid' : '' }}" type="text" name="como_nos_conheceu" id="como_nos_conheceu" value="{{ old('como_nos_conheceu', $contato->como_nos_conheceu) }}">
                @if($errors->has('como_nos_conheceu'))
                    <span class="text-danger">{{ $errors->first('como_nos_conheceu') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.contato.fields.como_nos_conheceu_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="mensagem">{{ trans('cruds.contato.fields.mensagem') }}</label>
                <textarea class="form-control {{ $errors->has('mensagem') ? 'is-invalid' : '' }}" name="mensagem" id="mensagem">{{ old('mensagem', $contato->mensagem) }}</textarea>
                @if($errors->has('mensagem'))
                    <span class="text-danger">{{ $errors->first('mensagem') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.contato.fields.mensagem_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection