@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.detalhe.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.detalhes.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="categoria_id">{{ trans('cruds.detalhe.fields.categoria') }}</label>
                <select class="form-control select2 {{ $errors->has('categoria') ? 'is-invalid' : '' }}" name="categoria_id" id="categoria_id">
                    @foreach($categorias as $id => $categoria)
                        <option value="{{ $id }}" {{ old('categoria_id') == $id ? 'selected' : '' }}>{{ $categoria }}</option>
                    @endforeach
                </select>
                @if($errors->has('categoria_id'))
                    <span class="text-danger">{{ $errors->first('categoria_id') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.detalhe.fields.categoria_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="conteudo">{{ trans('cruds.detalhe.fields.conteudo') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('conteudo') ? 'is-invalid' : '' }}" name="conteudo" id="conteudo">{!! old('conteudo') !!}</textarea>
                @if($errors->has('conteudo'))
                    <span class="text-danger">{{ $errors->first('conteudo') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.detalhe.fields.conteudo_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="imagem">{{ trans('cruds.detalhe.fields.imagem') }}</label>
                <div class="needsclick dropzone {{ $errors->has('imagem') ? 'is-invalid' : '' }}" id="imagem-dropzone">
                </div>
                @if($errors->has('imagem'))
                    <span class="text-danger">{{ $errors->first('imagem') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.detalhe.fields.imagem_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/admin/detalhes/ckmedia', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', {{ $detalhe->id ?? 0 }});
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

<script>
    Dropzone.options.imagemDropzone = {
    url: '{{ route('admin.detalhes.storeMedia') }}',
    maxFilesize: 2, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 2,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="imagem"]').remove()
      $('form').append('<input type="hidden" name="imagem" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="imagem"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($detalhe) && $detalhe->imagem)
      var file = {!! json_encode($detalhe->imagem) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, '{{ $detalhe->imagem->getUrl('thumb') }}')
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="imagem" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}
</script>
@endsection