@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.detalhe.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.detalhes.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.detalhe.fields.id') }}
                        </th>
                        <td>
                            {{ $detalhe->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.detalhe.fields.categoria') }}
                        </th>
                        <td>
                            {{ $detalhe->categoria->nome ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.detalhe.fields.conteudo') }}
                        </th>
                        <td>
                            {!! $detalhe->conteudo !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.detalhe.fields.imagem') }}
                        </th>
                        <td>
                            @if($detalhe->imagem)
                                <a href="{{ $detalhe->imagem->getUrl() }}" target="_blank">
                                    <img src="{{ $detalhe->imagem->getUrl('thumb') }}" width="50px" height="50px">
                                </a>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.detalhes.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection