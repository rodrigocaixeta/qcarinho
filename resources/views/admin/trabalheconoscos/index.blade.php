@extends('layouts.admin')
@section('content')
@can('trabalheconosco_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.trabalheconoscos.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.trabalheconosco.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.trabalheconosco.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Trabalheconosco">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.trabalheconosco.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.trabalheconosco.fields.nome') }}
                        </th>
                        <th>
                            {{ trans('cruds.trabalheconosco.fields.telefone') }}
                        </th>
                        <th>
                            {{ trans('cruds.trabalheconosco.fields.curriculo') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($trabalheconoscos as $key => $trabalheconosco)
                        <tr data-entry-id="{{ $trabalheconosco->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $trabalheconosco->id ?? '' }}
                            </td>
                            <td>
                                {{ $trabalheconosco->nome ?? '' }}
                            </td>
                            <td>
                                {{ $trabalheconosco->telefone ?? '' }}
                            </td>
                            <td>
                                @if($trabalheconosco->curriculo)
                                    <a href="{{ $trabalheconosco->curriculo->getUrl() }}" target="_blank">
                                        {{ trans('global.view_file') }}
                                    </a>
                                @endif
                            </td>
                            <td>
                                @can('trabalheconosco_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.trabalheconoscos.show', $trabalheconosco->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('trabalheconosco_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.trabalheconoscos.edit', $trabalheconosco->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('trabalheconosco_delete')
                                    <form action="{{ route('admin.trabalheconoscos.destroy', $trabalheconosco->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('trabalheconosco_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.trabalheconoscos.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-Trabalheconosco:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection