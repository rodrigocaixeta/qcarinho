@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.trabalheconosco.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.trabalheconoscos.update", [$trabalheconosco->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="nome">{{ trans('cruds.trabalheconosco.fields.nome') }}</label>
                <input class="form-control {{ $errors->has('nome') ? 'is-invalid' : '' }}" type="text" name="nome" id="nome" value="{{ old('nome', $trabalheconosco->nome) }}">
                @if($errors->has('nome'))
                    <span class="text-danger">{{ $errors->first('nome') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.trabalheconosco.fields.nome_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="telefone">{{ trans('cruds.trabalheconosco.fields.telefone') }}</label>
                <input class="form-control {{ $errors->has('telefone') ? 'is-invalid' : '' }}" type="text" name="telefone" id="telefone" value="{{ old('telefone', $trabalheconosco->telefone) }}">
                @if($errors->has('telefone'))
                    <span class="text-danger">{{ $errors->first('telefone') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.trabalheconosco.fields.telefone_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="curriculo">{{ trans('cruds.trabalheconosco.fields.curriculo') }}</label>
                <div class="needsclick dropzone {{ $errors->has('curriculo') ? 'is-invalid' : '' }}" id="curriculo-dropzone">
                </div>
                @if($errors->has('curriculo'))
                    <span class="text-danger">{{ $errors->first('curriculo') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.trabalheconosco.fields.curriculo_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    Dropzone.options.curriculoDropzone = {
    url: '{{ route('admin.trabalheconoscos.storeMedia') }}',
    maxFilesize: 2, // MB
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 2
    },
    success: function (file, response) {
      $('form').find('input[name="curriculo"]').remove()
      $('form').append('<input type="hidden" name="curriculo" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="curriculo"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($trabalheconosco) && $trabalheconosco->curriculo)
      var file = {!! json_encode($trabalheconosco->curriculo) !!}
          this.options.addedfile.call(this, file)
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="curriculo" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
}
</script>
@endsection