@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.trabalheconosco.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.trabalheconoscos.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.trabalheconosco.fields.id') }}
                        </th>
                        <td>
                            {{ $trabalheconosco->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.trabalheconosco.fields.nome') }}
                        </th>
                        <td>
                            {{ $trabalheconosco->nome }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.trabalheconosco.fields.telefone') }}
                        </th>
                        <td>
                            {{ $trabalheconosco->telefone }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.trabalheconosco.fields.curriculo') }}
                        </th>
                        <td>
                            @if($trabalheconosco->curriculo)
                                <a href="{{ $trabalheconosco->curriculo->getUrl() }}" target="_blank">
                                    {{ trans('global.view_file') }}
                                </a>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.trabalheconoscos.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection