<aside class="main-sidebar sidebar-dark-primary elevation-4" style="min-height: 917px;">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <span class="brand-text font-weight-light">{{ trans('panel.site_title') }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route("admin.home") }}" class="nav-link">
                        <p>
                            <i class="fas fa-fw fa-tachometer-alt">

                            </i>
                            <span>{{ trans('global.dashboard') }}</span>
                        </p>
                    </a>
                </li>
                @can('user_management_access')
                    <li class="nav-item has-treeview {{ request()->is('admin/permissions*') ? 'menu-open' : '' }} {{ request()->is('admin/roles*') ? 'menu-open' : '' }} {{ request()->is('admin/users*') ? 'menu-open' : '' }}">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="fa-fw fas fa-users">

                            </i>
                            <p>
                                <span>{{ trans('cruds.userManagement.title') }}</span>
                                <i class="right fa fa-fw fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('permission_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.permissions.index") }}" class="nav-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-unlock-alt">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.permission.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('role_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.roles.index") }}" class="nav-link {{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-briefcase">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.role.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('user_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.users.index") }}" class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-user">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.user.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('slideshow_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.slideshows.index") }}" class="nav-link {{ request()->is('admin/slideshows') || request()->is('admin/slideshows/*') ? 'active' : '' }}">
                            <i class="fa-fw fas fa-image">

                            </i>
                            <p>
                                <span>{{ trans('cruds.slideshow.title') }}</span>
                            </p>
                        </a>
                    </li>
                @endcan
                @can('sobre_escola_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.sobre-escolas.index") }}" class="nav-link {{ request()->is('admin/sobre-escolas') || request()->is('admin/sobre-escolas/*') ? 'active' : '' }}">
                            <i class="fa-fw fas fa-align-justify">

                            </i>
                            <p>
                                <span>{{ trans('cruds.sobreEscola.title') }}</span>
                            </p>
                        </a>
                    </li>
                @endcan
                @can('escola_access')
                    <li class="nav-item has-treeview {{ request()->is('admin/categoria*') ? 'menu-open' : '' }} {{ request()->is('admin/detalhes*') ? 'menu-open' : '' }}">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="fa-fw fas fa-cogs">

                            </i>
                            <p>
                                <span>{{ trans('cruds.escola.title') }}</span>
                                <i class="right fa fa-fw fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('categorium_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.categoria.index") }}" class="nav-link {{ request()->is('admin/categoria') || request()->is('admin/categoria/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-sticky-note">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.categorium.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('detalhe_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.detalhes.index") }}" class="nav-link {{ request()->is('admin/detalhes') || request()->is('admin/detalhes/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-align-left">

                                        </i>
                                        <p>
                                            <span>{{ trans('cruds.detalhe.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('blog_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.blogs.index") }}" class="nav-link {{ request()->is('admin/blogs') || request()->is('admin/blogs/*') ? 'active' : '' }}">
                            <i class="fa-fw fab fa-blogger-b">

                            </i>
                            <p>
                                <span>{{ trans('cruds.blog.title') }}</span>
                            </p>
                        </a>
                    </li>
                @endcan
                @can('contato_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.contatos.index") }}" class="nav-link {{ request()->is('admin/contatos') || request()->is('admin/contatos/*') ? 'active' : '' }}">
                            <i class="fa-fw fab fa-contao">

                            </i>
                            <p>
                                <span>{{ trans('cruds.contato.title') }}</span>
                            </p>
                        </a>
                    </li>
                @endcan
                @can('parceiro_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.parceiros.index") }}" class="nav-link {{ request()->is('admin/parceiros') || request()->is('admin/parceiros/*') ? 'active' : '' }}">
                            <i class="fa-fw fas fa-handshake">

                            </i>
                            <p>
                                <span>{{ trans('cruds.parceiro.title') }}</span>
                            </p>
                        </a>
                    </li>
                @endcan
                @can('trabalheconosco_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.trabalheconoscos.index") }}" class="nav-link {{ request()->is('admin/trabalheconoscos') || request()->is('admin/trabalheconoscos/*') ? 'active' : '' }}">
                            <i class="fa-fw fas fa-cogs">

                            </i>
                            <p>
                                <span>{{ trans('cruds.trabalheconosco.title') }}</span>
                            </p>
                        </a>
                    </li>
                @endcan
                <li class="nav-item">
                    <a href="#" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                        <p>
                            <i class="fas fa-fw fa-sign-out-alt">

                            </i>
                            <span>{{ trans('global.logout') }}</span>
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>