<!-- Modal -->
<div class="modal fade" id="trabalhec" tabindex="-1" role="dialog" aria-labelledby="trabalhec" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Trabalhe Conosco</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="trabalheconosco" action="{{ route("trabalheconosco") }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="nome2">{{ trans('cruds.trabalheconosco.fields.nome') }}</label>
                        <input class="form-control {{ $errors->has('nome') ? 'is-invalid' : '' }}" type="text" name="nome" id="nome2" value="{{ old('nome', '') }}">
                        @if($errors->has('nome'))
                            <span class="text-danger">{{ $errors->first('nome') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.trabalheconosco.fields.nome_helper') }}</span>
                    </div>
                    <div class="form-group">
                        <label for="telefone2">{{ trans('cruds.trabalheconosco.fields.telefone') }}</label>
                        <input class="form-control {{ $errors->has('telefone') ? 'is-invalid' : '' }}" type="text" name="telefone" id="telefone2" value="{{ old('telefone', '') }}">
                        @if($errors->has('telefone'))
                            <span class="text-danger">{{ $errors->first('telefone') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.trabalheconosco.fields.telefone_helper') }}</span>
                    </div>
                    <div class="form-group">
                        <label for="curriculo">{{ trans('cruds.trabalheconosco.fields.curriculo') }}</label>
                        <div class="needsclick dropzone {{ $errors->has('curriculo') ? 'is-invalid' : '' }}" id="curriculo-dropzone">
                        </div>
                        @if($errors->has('curriculo'))
                            <span class="text-danger">{{ $errors->first('curriculo') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.trabalheconosco.fields.curriculo_helper') }}</span>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary btn-block" type="submit">
                            Enviar
                        </button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

