<!-- Modal -->
<div class="modal fade" id="blog{{$b->id}}" tabindex="-1" role="dialog" aria-labelledby="blog{{$b->id}}" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{$b->titulo}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="text-center"><img src="{{url($b->imagem->getUrl())}}" class="img-fluid" alt=""></p>
                {!! $b->conteudo !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
