<!-- Modal -->
<div class="modal fade" id="blogs" tabindex="-1" role="dialog" aria-labelledby="blogs" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Blog</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <section id="blog">
                    <div class="container">
                        <div class="row py-5">
                            @foreach ($blogs as $b)

                                <div class="col-md-4">
                                    <img src="{{url($b->imagem->getUrl())}}" class="img-fluid" alt="">
                                    <a href="#" onclick="$('#blogs').modal('hide');" data-toggle="modal" data-target="#blog{{$b->id}}"><h4>{{$b->titulo}}</h4></a>
                                    <p>{{Str::limit($b->introducao, 200, ' (...)')}}</p>
                                    <p class="data">{{\Carbon\Carbon::parse($b->created_at)->format('d/m/Y')}}</p>
                                    @include('modal.blog')
                                </div>

                            @endforeach

                        </div>
                    </div>
                </section>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
