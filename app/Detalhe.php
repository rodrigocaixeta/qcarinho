<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Detalhe extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public $table = 'detalhes';

    protected $appends = [
        'imagem',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'conteudo',
        'created_at',
        'updated_at',
        'deleted_at',
        'categoria_id',
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function categoria()
    {
        return $this->belongsTo(Categorium::class, 'categoria_id');
    }

    public function getImagemAttribute()
    {
        $file = $this->getMedia('imagem')->last();

        if ($file) {
            $file->url       = $file->getUrl();
            $file->thumbnail = $file->getUrl('thumb');
        }

        return $file;
    }
}
