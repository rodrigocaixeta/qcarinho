<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreSobreEscolaRequest;
use App\Http\Requests\UpdateSobreEscolaRequest;
use App\Http\Resources\Admin\SobreEscolaResource;
use App\SobreEscola;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SobreEscolaApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('sobre_escola_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new SobreEscolaResource(SobreEscola::all());
    }

    public function store(StoreSobreEscolaRequest $request)
    {
        $sobreEscola = SobreEscola::create($request->all());

        return (new SobreEscolaResource($sobreEscola))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(SobreEscola $sobreEscola)
    {
        abort_if(Gate::denies('sobre_escola_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new SobreEscolaResource($sobreEscola);
    }

    public function update(UpdateSobreEscolaRequest $request, SobreEscola $sobreEscola)
    {
        $sobreEscola->update($request->all());

        return (new SobreEscolaResource($sobreEscola))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(SobreEscola $sobreEscola)
    {
        abort_if(Gate::denies('sobre_escola_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $sobreEscola->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
