<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreSlideshowRequest;
use App\Http\Requests\UpdateSlideshowRequest;
use App\Http\Resources\Admin\SlideshowResource;
use App\Slideshow;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SlideshowApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('slideshow_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new SlideshowResource(Slideshow::all());
    }

    public function store(StoreSlideshowRequest $request)
    {
        $slideshow = Slideshow::create($request->all());

        if ($request->input('imagem', false)) {
            $slideshow->addMedia(storage_path('tmp/uploads/' . $request->input('imagem')))->toMediaCollection('imagem');
        }

        return (new SlideshowResource($slideshow))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Slideshow $slideshow)
    {
        abort_if(Gate::denies('slideshow_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new SlideshowResource($slideshow);
    }

    public function update(UpdateSlideshowRequest $request, Slideshow $slideshow)
    {
        $slideshow->update($request->all());

        if ($request->input('imagem', false)) {
            if (!$slideshow->imagem || $request->input('imagem') !== $slideshow->imagem->file_name) {
                $slideshow->addMedia(storage_path('tmp/uploads/' . $request->input('imagem')))->toMediaCollection('imagem');
            }
        } elseif ($slideshow->imagem) {
            $slideshow->imagem->delete();
        }

        return (new SlideshowResource($slideshow))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Slideshow $slideshow)
    {
        abort_if(Gate::denies('slideshow_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $slideshow->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
