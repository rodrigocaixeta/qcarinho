<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Detalhe;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreDetalheRequest;
use App\Http\Requests\UpdateDetalheRequest;
use App\Http\Resources\Admin\DetalheResource;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DetalhesApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('detalhe_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new DetalheResource(Detalhe::with(['categoria'])->get());
    }

    public function store(StoreDetalheRequest $request)
    {
        $detalhe = Detalhe::create($request->all());

        if ($request->input('imagem', false)) {
            $detalhe->addMedia(storage_path('tmp/uploads/' . $request->input('imagem')))->toMediaCollection('imagem');
        }

        return (new DetalheResource($detalhe))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Detalhe $detalhe)
    {
        abort_if(Gate::denies('detalhe_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new DetalheResource($detalhe->load(['categoria']));
    }

    public function update(UpdateDetalheRequest $request, Detalhe $detalhe)
    {
        $detalhe->update($request->all());

        if ($request->input('imagem', false)) {
            if (!$detalhe->imagem || $request->input('imagem') !== $detalhe->imagem->file_name) {
                $detalhe->addMedia(storage_path('tmp/uploads/' . $request->input('imagem')))->toMediaCollection('imagem');
            }
        } elseif ($detalhe->imagem) {
            $detalhe->imagem->delete();
        }

        return (new DetalheResource($detalhe))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Detalhe $detalhe)
    {
        abort_if(Gate::denies('detalhe_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $detalhe->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
