<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreParceiroRequest;
use App\Http\Requests\UpdateParceiroRequest;
use App\Http\Resources\Admin\ParceiroResource;
use App\Parceiro;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ParceirosApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('parceiro_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ParceiroResource(Parceiro::all());
    }

    public function store(StoreParceiroRequest $request)
    {
        $parceiro = Parceiro::create($request->all());

        if ($request->input('parceiro', false)) {
            $parceiro->addMedia(storage_path('tmp/uploads/' . $request->input('parceiro')))->toMediaCollection('parceiro');
        }

        return (new ParceiroResource($parceiro))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Parceiro $parceiro)
    {
        abort_if(Gate::denies('parceiro_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ParceiroResource($parceiro);
    }

    public function update(UpdateParceiroRequest $request, Parceiro $parceiro)
    {
        $parceiro->update($request->all());

        if ($request->input('parceiro', false)) {
            if (!$parceiro->parceiro || $request->input('parceiro') !== $parceiro->parceiro->file_name) {
                $parceiro->addMedia(storage_path('tmp/uploads/' . $request->input('parceiro')))->toMediaCollection('parceiro');
            }
        } elseif ($parceiro->parceiro) {
            $parceiro->parceiro->delete();
        }

        return (new ParceiroResource($parceiro))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Parceiro $parceiro)
    {
        abort_if(Gate::denies('parceiro_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $parceiro->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
