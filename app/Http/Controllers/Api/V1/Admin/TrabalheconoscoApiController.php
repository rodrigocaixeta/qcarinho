<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreTrabalheconoscoRequest;
use App\Http\Requests\UpdateTrabalheconoscoRequest;
use App\Http\Resources\Admin\TrabalheconoscoResource;
use App\Trabalheconosco;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TrabalheconoscoApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('trabalheconosco_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new TrabalheconoscoResource(Trabalheconosco::all());
    }

    public function store(StoreTrabalheconoscoRequest $request)
    {
        $trabalheconosco = Trabalheconosco::create($request->all());

        if ($request->input('curriculo', false)) {
            $trabalheconosco->addMedia(storage_path('tmp/uploads/' . $request->input('curriculo')))->toMediaCollection('curriculo');
        }

        return (new TrabalheconoscoResource($trabalheconosco))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Trabalheconosco $trabalheconosco)
    {
        abort_if(Gate::denies('trabalheconosco_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new TrabalheconoscoResource($trabalheconosco);
    }

    public function update(UpdateTrabalheconoscoRequest $request, Trabalheconosco $trabalheconosco)
    {
        $trabalheconosco->update($request->all());

        if ($request->input('curriculo', false)) {
            if (!$trabalheconosco->curriculo || $request->input('curriculo') !== $trabalheconosco->curriculo->file_name) {
                $trabalheconosco->addMedia(storage_path('tmp/uploads/' . $request->input('curriculo')))->toMediaCollection('curriculo');
            }
        } elseif ($trabalheconosco->curriculo) {
            $trabalheconosco->curriculo->delete();
        }

        return (new TrabalheconoscoResource($trabalheconosco))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Trabalheconosco $trabalheconosco)
    {
        abort_if(Gate::denies('trabalheconosco_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $trabalheconosco->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
