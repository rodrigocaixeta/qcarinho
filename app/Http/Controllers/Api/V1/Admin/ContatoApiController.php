<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Contato;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreContatoRequest;
use App\Http\Requests\UpdateContatoRequest;
use App\Http\Resources\Admin\ContatoResource;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ContatoApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('contato_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ContatoResource(Contato::all());
    }

    public function store(StoreContatoRequest $request)
    {
        $contato = Contato::create($request->all());

        return (new ContatoResource($contato))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Contato $contato)
    {
        abort_if(Gate::denies('contato_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ContatoResource($contato);
    }

    public function update(UpdateContatoRequest $request, Contato $contato)
    {
        $contato->update($request->all());

        return (new ContatoResource($contato))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Contato $contato)
    {
        abort_if(Gate::denies('contato_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $contato->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
