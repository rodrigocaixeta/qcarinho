<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroySobreEscolaRequest;
use App\Http\Requests\StoreSobreEscolaRequest;
use App\Http\Requests\UpdateSobreEscolaRequest;
use App\SobreEscola;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class SobreEscolaController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('sobre_escola_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $sobreEscolas = SobreEscola::all();

        return view('admin.sobreEscolas.index', compact('sobreEscolas'));
    }

    public function create()
    {
        abort_if(Gate::denies('sobre_escola_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.sobreEscolas.create');
    }

    public function store(StoreSobreEscolaRequest $request)
    {
        $sobreEscola = SobreEscola::create($request->all());

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $sobreEscola->id]);
        }

        return redirect()->route('admin.sobre-escolas.index');
    }

    public function edit(SobreEscola $sobreEscola)
    {
        abort_if(Gate::denies('sobre_escola_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.sobreEscolas.edit', compact('sobreEscola'));
    }

    public function update(UpdateSobreEscolaRequest $request, SobreEscola $sobreEscola)
    {
        $sobreEscola->update($request->all());

        return redirect()->route('admin.sobre-escolas.index');
    }

    public function show(SobreEscola $sobreEscola)
    {
        abort_if(Gate::denies('sobre_escola_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.sobreEscolas.show', compact('sobreEscola'));
    }

    public function destroy(SobreEscola $sobreEscola)
    {
        abort_if(Gate::denies('sobre_escola_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $sobreEscola->delete();

        return back();
    }

    public function massDestroy(MassDestroySobreEscolaRequest $request)
    {
        SobreEscola::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('sobre_escola_create') && Gate::denies('sobre_escola_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new SobreEscola();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media', 'public');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
