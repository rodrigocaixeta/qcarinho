<?php

namespace App\Http\Controllers\Admin;

use App\Contato;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyContatoRequest;
use App\Http\Requests\StoreContatoRequest;
use App\Http\Requests\UpdateContatoRequest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ContatoController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('contato_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $contatos = Contato::all();

        return view('admin.contatos.index', compact('contatos'));
    }

    public function create()
    {
        abort_if(Gate::denies('contato_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.contatos.create');
    }

    public function store(StoreContatoRequest $request)
    {
        $contato = Contato::create($request->all());

        return redirect()->route('admin.contatos.index');
    }

    public function edit(Contato $contato)
    {
        abort_if(Gate::denies('contato_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.contatos.edit', compact('contato'));
    }

    public function update(UpdateContatoRequest $request, Contato $contato)
    {
        $contato->update($request->all());

        return redirect()->route('admin.contatos.index');
    }

    public function show(Contato $contato)
    {
        abort_if(Gate::denies('contato_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.contatos.show', compact('contato'));
    }

    public function destroy(Contato $contato)
    {
        abort_if(Gate::denies('contato_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $contato->delete();

        return back();
    }

    public function massDestroy(MassDestroyContatoRequest $request)
    {
        Contato::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
