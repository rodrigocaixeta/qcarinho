<?php

namespace App\Http\Controllers\Admin;

use App\Categorium;
use App\Detalhe;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyDetalheRequest;
use App\Http\Requests\StoreDetalheRequest;
use App\Http\Requests\UpdateDetalheRequest;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class DetalhesController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('detalhe_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $detalhes = Detalhe::all();

        return view('admin.detalhes.index', compact('detalhes'));
    }

    public function create()
    {
        abort_if(Gate::denies('detalhe_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categorias = Categorium::all()->pluck('nome', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.detalhes.create', compact('categorias'));
    }

    public function store(StoreDetalheRequest $request)
    {
        $detalhe = Detalhe::create($request->all());

        if ($request->input('imagem', false)) {
            $detalhe->addMedia(storage_path('tmp/uploads/' . $request->input('imagem')))->toMediaCollection('imagem');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $detalhe->id]);
        }

        return redirect()->route('admin.detalhes.index');
    }

    public function edit(Detalhe $detalhe)
    {
        abort_if(Gate::denies('detalhe_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categorias = Categorium::all()->pluck('nome', 'id')->prepend(trans('global.pleaseSelect'), '');

        $detalhe->load('categoria');

        return view('admin.detalhes.edit', compact('categorias', 'detalhe'));
    }

    public function update(UpdateDetalheRequest $request, Detalhe $detalhe)
    {
        $detalhe->update($request->all());

        if ($request->input('imagem', false)) {
            if (!$detalhe->imagem || $request->input('imagem') !== $detalhe->imagem->file_name) {
                $detalhe->addMedia(storage_path('tmp/uploads/' . $request->input('imagem')))->toMediaCollection('imagem');
            }
        } elseif ($detalhe->imagem) {
            $detalhe->imagem->delete();
        }

        return redirect()->route('admin.detalhes.index');
    }

    public function show(Detalhe $detalhe)
    {
        abort_if(Gate::denies('detalhe_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $detalhe->load('categoria');

        return view('admin.detalhes.show', compact('detalhe'));
    }

    public function destroy(Detalhe $detalhe)
    {
        abort_if(Gate::denies('detalhe_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $detalhe->delete();

        return back();
    }

    public function massDestroy(MassDestroyDetalheRequest $request)
    {
        Detalhe::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('detalhe_create') && Gate::denies('detalhe_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Detalhe();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media', 'public');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
