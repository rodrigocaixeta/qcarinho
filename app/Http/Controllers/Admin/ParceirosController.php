<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyParceiroRequest;
use App\Http\Requests\StoreParceiroRequest;
use App\Http\Requests\UpdateParceiroRequest;
use App\Parceiro;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class ParceirosController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('parceiro_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $parceiros = Parceiro::all();

        return view('admin.parceiros.index', compact('parceiros'));
    }

    public function create()
    {
        abort_if(Gate::denies('parceiro_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.parceiros.create');
    }

    public function store(StoreParceiroRequest $request)
    {
        $parceiro = Parceiro::create($request->all());

        if ($request->input('parceiro', false)) {
            $parceiro->addMedia(storage_path('tmp/uploads/' . $request->input('parceiro')))->toMediaCollection('parceiro');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $parceiro->id]);
        }

        return redirect()->route('admin.parceiros.index');
    }

    public function edit(Parceiro $parceiro)
    {
        abort_if(Gate::denies('parceiro_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.parceiros.edit', compact('parceiro'));
    }

    public function update(UpdateParceiroRequest $request, Parceiro $parceiro)
    {
        $parceiro->update($request->all());

        if ($request->input('parceiro', false)) {
            if (!$parceiro->parceiro || $request->input('parceiro') !== $parceiro->parceiro->file_name) {
                $parceiro->addMedia(storage_path('tmp/uploads/' . $request->input('parceiro')))->toMediaCollection('parceiro');
            }
        } elseif ($parceiro->parceiro) {
            $parceiro->parceiro->delete();
        }

        return redirect()->route('admin.parceiros.index');
    }

    public function show(Parceiro $parceiro)
    {
        abort_if(Gate::denies('parceiro_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.parceiros.show', compact('parceiro'));
    }

    public function destroy(Parceiro $parceiro)
    {
        abort_if(Gate::denies('parceiro_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $parceiro->delete();

        return back();
    }

    public function massDestroy(MassDestroyParceiroRequest $request)
    {
        Parceiro::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('parceiro_create') && Gate::denies('parceiro_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Parceiro();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media', 'public');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
