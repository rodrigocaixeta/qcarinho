<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyTrabalheconoscoRequest;
use App\Http\Requests\StoreTrabalheconoscoRequest;
use App\Http\Requests\UpdateTrabalheconoscoRequest;
use App\Trabalheconosco;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class TrabalheconoscoController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('trabalheconosco_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $trabalheconoscos = Trabalheconosco::all();

        return view('admin.trabalheconoscos.index', compact('trabalheconoscos'));
    }

    public function create()
    {
        abort_if(Gate::denies('trabalheconosco_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.trabalheconoscos.create');
    }

    public function store(StoreTrabalheconoscoRequest $request)
    {
        $trabalheconosco = Trabalheconosco::create($request->all());

        if ($request->input('curriculo', false)) {
            $trabalheconosco->addMedia(storage_path('tmp/uploads/' . $request->input('curriculo')))->toMediaCollection('curriculo');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $trabalheconosco->id]);
        }

        return redirect()->route('admin.trabalheconoscos.index');
    }

    public function edit(Trabalheconosco $trabalheconosco)
    {
        abort_if(Gate::denies('trabalheconosco_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.trabalheconoscos.edit', compact('trabalheconosco'));
    }

    public function update(UpdateTrabalheconoscoRequest $request, Trabalheconosco $trabalheconosco)
    {
        $trabalheconosco->update($request->all());

        if ($request->input('curriculo', false)) {
            if (!$trabalheconosco->curriculo || $request->input('curriculo') !== $trabalheconosco->curriculo->file_name) {
                $trabalheconosco->addMedia(storage_path('tmp/uploads/' . $request->input('curriculo')))->toMediaCollection('curriculo');
            }
        } elseif ($trabalheconosco->curriculo) {
            $trabalheconosco->curriculo->delete();
        }

        return redirect()->route('admin.trabalheconoscos.index');
    }

    public function show(Trabalheconosco $trabalheconosco)
    {
        abort_if(Gate::denies('trabalheconosco_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.trabalheconoscos.show', compact('trabalheconosco'));
    }

    public function destroy(Trabalheconosco $trabalheconosco)
    {
        abort_if(Gate::denies('trabalheconosco_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $trabalheconosco->delete();

        return back();
    }

    public function massDestroy(MassDestroyTrabalheconoscoRequest $request)
    {
        Trabalheconosco::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('trabalheconosco_create') && Gate::denies('trabalheconosco_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Trabalheconosco();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media', 'public');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
