<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroySlideshowRequest;
use App\Http\Requests\StoreSlideshowRequest;
use App\Http\Requests\UpdateSlideshowRequest;
use App\Slideshow;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SlideshowController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('slideshow_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $slideshows = Slideshow::all();

        return view('admin.slideshows.index', compact('slideshows'));
    }

    public function create()
    {
        abort_if(Gate::denies('slideshow_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.slideshows.create');
    }

    public function store(StoreSlideshowRequest $request)
    {
        $slideshow = Slideshow::create($request->all());

        if ($request->input('imagem', false)) {
            $slideshow->addMedia(storage_path('tmp/uploads/' . $request->input('imagem')))->toMediaCollection('imagem');
        }

        return redirect()->route('admin.slideshows.index');
    }

    public function edit(Slideshow $slideshow)
    {
        abort_if(Gate::denies('slideshow_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.slideshows.edit', compact('slideshow'));
    }

    public function update(UpdateSlideshowRequest $request, Slideshow $slideshow)
    {
        $slideshow->update($request->all());

        if ($request->input('imagem', false)) {
            if (!$slideshow->imagem || $request->input('imagem') !== $slideshow->imagem->file_name) {
                $slideshow->addMedia(storage_path('tmp/uploads/' . $request->input('imagem')))->toMediaCollection('imagem');
            }
        } elseif ($slideshow->imagem) {
            $slideshow->imagem->delete();
        }

        return redirect()->route('admin.slideshows.index');
    }

    public function show(Slideshow $slideshow)
    {
        abort_if(Gate::denies('slideshow_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.slideshows.show', compact('slideshow'));
    }

    public function destroy(Slideshow $slideshow)
    {
        abort_if(Gate::denies('slideshow_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $slideshow->delete();

        return back();
    }

    public function massDestroy(MassDestroySlideshowRequest $request)
    {
        Slideshow::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
