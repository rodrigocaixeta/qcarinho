<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Categorium;
use App\Contato;
use App\Detalhe;
use App\Http\Requests\StoreContatoRequest;
use App\Http\Requests\StoreTrabalheconoscoRequest;
use App\Parceiro;
use App\Slideshow;
use App\SobreEscola;
use App\Trabalheconosco;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;

class SiteController extends Controller
{
    public function index() {
        $slide = Slideshow::all();
        $qcarinho = SobreEscola::first();
        $servicos = Detalhe::all();
        $categorias = Categorium::all();
        $blog = Blog::paginate(3);
        $blogs = Blog::all();
        $parceiros = Parceiro::paginate(3);
        return view('site.home', compact('slide','qcarinho', 'categorias', 'servicos', 'blog', 'blogs', 'parceiros'));
    }

    public function trabalheconosco(StoreTrabalheconoscoRequest $request)
    {
        $trabalheconosco = Trabalheconosco::create($request->all());

        if ($request->input('curriculo', false)) {
            $trabalheconosco->addMedia(storage_path('tmp/uploads/' . $request->input('curriculo')))->toMediaCollection('curriculo');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $trabalheconosco->id]);
        }

        return redirect()->route('site');
    }

    public function contato(StoreContatoRequest $request)
    {
        $contato = Contato::create($request->all());

        return redirect()->route('site');
    }
}
