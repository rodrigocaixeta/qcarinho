<?php

namespace App\Http\Requests;

use App\Detalhe;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreDetalheRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('detalhe_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
        ];
    }
}
