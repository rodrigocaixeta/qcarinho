<?php

namespace App\Http\Requests;

use App\SobreEscola;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroySobreEscolaRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('sobre_escola_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:sobre_escolas,id',
        ];
    }
}
