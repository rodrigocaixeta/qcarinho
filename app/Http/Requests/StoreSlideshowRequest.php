<?php

namespace App\Http\Requests;

use App\Slideshow;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreSlideshowRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('slideshow_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
        ];
    }
}
