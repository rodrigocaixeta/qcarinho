<?php

namespace App\Http\Requests;

use App\Contato;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateContatoRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('contato_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
        ];
    }
}
