<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categorium extends Model
{
    use SoftDeletes;

    public $table = 'categoria';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'nome',
        'slug',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function categoriaDetalhes()
    {
        return $this->hasMany(Detalhe::class, 'categoria_id', 'id');
    }
}
